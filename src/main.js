import Vue from 'vue'
import './plugins/vuetify'
import App from './App.vue'
import router from './router'
require('./assets/style.scss')
import * as VueGoogleMaps from 'vue2-google-maps'
const Axios = require('axios');

Vue.prototype.$axios = Axios.create({
  baseURL: process.env.NODE_ENV === 'development' ? 'https://try.milkup.com.br/' : '/',
  withCredentials: true
});

Vue.prototype.$qs = require('querystring');

Vue.use(VueGoogleMaps, {
  load: {
    key: 'AIzaSyBRL3el9-8ltnBBn7CMVy-cDh7a26zOwhc',
    libraries: 'places', // This is required if you use the Autocomplete plugin
    // OR: libraries: 'places,drawing'
    // OR: libraries: 'places,drawing,visualization'
    // (as you require)

    //// If you want to set the version, you can do so:
    // v: '3.26',
  }
});

Vue.config.productionTip = false

new Vue({
  router,
  render: h => h(App)
}).$mount('#app')
