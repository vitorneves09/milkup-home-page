import Vue from 'vue'
import Router from 'vue-router'

import Home from './views/Home.vue'
import Consultoria from './views/Consultoria.vue'
import Software from './views/Software.vue'
import Aplicativos from './views/Aplicativos.vue'
import Terceirizacao from './views/Terceirizacao.vue'
import Classificados from './views/Classificados.vue'
import Privacidade from './views/Privacidade.vue'

Vue.use(Router)

export default new Router({
  mode: 'history',
  base: process.env.BASE_URL,
  routes: [{
    path: '/',
    name: 'home',
    component: Home
  },
  {
    path: '/consultoria',
    name: 'consultoria',
    // component: () => import('./views/Consultoria.vue')
    component: Consultoria
  },
  {
    path: '/software',
    name: 'software',
    // component: () => import('./views/Software.vue')
    component: Software
  },
  {
    path: '/aplicativos',
    name: 'aplicativos',
    // component: () => import('./views/Aplicativos.vue')
    component: Aplicativos
  },
  {
    path: '/terceirizacao',
    name: 'terceirizacao',
    // component: () => import('./views/Terceirizacao.vue')
    component: Terceirizacao
  },
  {
    path: '/classificados',
    name: 'classificados',
    // component: () => import('./views/Classificados.vue')
    component: Classificados
  },
  {
    path: '/privacidade',
    name: 'privacidade',
    // component: () => import('./views/Privacidade.vue')
    component: Privacidade
  }
  ],
  scrollBehavior: function (to) {
    if (to.hash) {
      return {
        selector: to.hash
      }
    } else {
      return {
        x: 0,
        y: 0
      }
    }
  },
})