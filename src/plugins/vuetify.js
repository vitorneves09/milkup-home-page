import Vue from 'vue'
import 'vuetify/src/stylus/app.styl'

import Vuetify from 'vuetify/lib'

// import Vuetify, {
//   VApp, // required
//   VNavigationDrawer,
//   VFooter,
//   VToolbar,
//   VParallax,
//   VFadeTransition
// } from 'vuetify/lib'
// import { Ripple } from 'vuetify/lib/directives'

Vue.use(Vuetify, {
  iconfont: 'md'
  // components: {
  //   VApp,
  //   VNavigationDrawer,
  //   VFooter,
  //   VToolbar,
  //   VParallax,
  //   VFadeTransition
  // },
  // directives: {
  //   Ripple
  // }
})